using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Enemy", menuName = "New Enemy")]
public class EnemyInfo : ScriptableObject
{
    [SerializeField] private int maxHealth = 10;
     [SerializeField] private AudioClip hitSound;
    [SerializeField] private AudioClip  deadSound;

    public int MAXHealth => maxHealth;

    public AudioClip HitSound => hitSound;

    public AudioClip DeadSound => deadSound;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

