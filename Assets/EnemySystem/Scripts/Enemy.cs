using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Enemy : TargetHit
{
    [SerializeField] private EnemyInfo enemyInfo;

    private void Awake()
    {
        onHit += OnShootRecieve;
        onDead += OnDead;
    }


    private void OnDestroy()
    {
        onHit -= OnShootRecieve;
        onDead -= OnDead;
    }

    void Start()
    {
        maxHealth = enemyInfo.MAXHealth;
        currentHealth = MaxHealth;
    }

    private void OnDead()
    {
        // play death anim and destroy after
        Destroy(this.gameObject);
    }

    void OnShootRecieve(int damage)
    {
        currentHealth -= damage;
        Debug.Log("damage done " + damage);
        UpdateHealth();
    }
}