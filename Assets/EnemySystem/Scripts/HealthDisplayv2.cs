using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthDisplayv2 : MonoBehaviour
{
  [SerializeField] private TargetHit health;
  [SerializeField] private GameObject healtBarParent;
  [SerializeField] private Image healthBarImage;


  private void Awake()
  {
    health = GetComponent<TargetHit>();
    health.clientOnHealthUpdated += HandleHealthUpdated;
    healtBarParent.SetActive(true);

  }

  private void Start()
  {
    HandleHealthUpdated(health.MaxHealth, health.MaxHealth);
  }

  private void OnDestroy()
  {
    health.clientOnHealthUpdated -= HandleHealthUpdated;}

 
  
  // todo deletwe this
  private void OnMouseEnter()
  {
//healtBarParent.SetActive(true);
}

  private void OnMouseExit()
  {
  //  healtBarParent.SetActive(false);
  }

  private void HandleHealthUpdated(int currentHealth, int maxHealth)
  {
     healthBarImage.fillAmount = (float)currentHealth / 
                                maxHealth;
  }
  
}

 
