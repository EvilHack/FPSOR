using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AmmoDisplay : MonoBehaviour
{
      [SerializeField] private TMP_Text amountText;


    private void Awake()
    {
        FireGun.onReload +=  HandleAmmoAmountUpdated;
 
    }

 
    private void OnDestroy()
    {
        FireGun.onReload -= HandleAmmoAmountUpdated;}

 
  
    

    private void HandleAmmoAmountUpdated(int currentAmount, int totalAmount)
    {
        
        amountText.text = " Amount:  " +currentAmount +  " | "  + totalAmount;
    }
}
