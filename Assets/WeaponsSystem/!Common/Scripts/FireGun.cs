using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.TextCore;

public class FireGun : MonoBehaviour
{
    [Header("Weapon Data")] [SerializeField]
    private WeaponData weapon;

    private float maxAmmo;
    private int totalAmmo = 80;
    private int currentChargedAmmo = 0;
    private int maxChargedAmmo = 20;


    private int currentDamage;

    public static Action<int, int> onReload;
    public static Action<FireGun> onDiscard;
    public   Action onInsufficientAmmo;
    public Action onShoot;
    private int shootCount = 0;

    private bool isFiring = false;

    [Header("Scpecific Current Frame Checkers")]
    private FireGunState previousFireGunState;

    private FireGunState currentFireGunState;

    private void Awake()
    {
    }

    void Start()
    {
        maxChargedAmmo = weapon.MAXChargerAmmo;
        maxAmmo = weapon.MAXAmmo;
        currentDamage = weapon.Damage;
        previousFireGunState = FireGunState.Idle;
        currentFireGunState = previousFireGunState;
    }

    void Update()
    {
        //todo arreglar esto
        if (currentFireGunState == previousFireGunState && currentFireGunState != FireGunState.Shooting)
        {
            ChangeCurrentState(FireGunState.Idle);
        }
    }

    public void IncreaseAmmo(int increaseQuantity)
    {
        totalAmmo += increaseQuantity;
        if (maxAmmo < currentChargedAmmo)
        {
            currentChargedAmmo = (int)maxAmmo;
        }

        onReload?.Invoke(currentChargedAmmo, totalAmmo);
    }


    public virtual void TryToShoot()
    {
        if (!isFiring && currentFireGunState != FireGunState.Reloading)
        {
            StartCoroutine(nameof(Fire));
        }

        switch (weapon.TypeOfShoot)
        {
            case TypeOfShoot.Manual:

                break;
            case TypeOfShoot.SemiAutomatic:
                break;
            case TypeOfShoot.Automatic:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

   

    public IEnumerator Fire()
    {
        isFiring = true;
        while (isFiring)
        {
            if (CanFire())
            {
                Shoot();
            }
            else
            {
                Reload();
                isFiring = false;
            }
            yield return new WaitForSeconds(weapon.RecoilOnShoot);
        }
        yield return null;
    }
    private  bool CanFire()
    {
        return currentChargedAmmo > 0;
    }
    private void Shoot()
    {
        if (shootCount >= weapon.MAXAmmoPerShoot)
        {
            Debug.Log("Te pasaste disparando " + shootCount);
            return;
        }

        ChangeCurrentState(FireGunState.Shooting);
        GameObject gameObject = Instantiate<GameObject>(weapon.Bullet);

        currentChargedAmmo--;

        onShoot?.Invoke();
        onReload?.Invoke(currentChargedAmmo, totalAmmo);
        shootCount++;
    }


    public virtual void Reload()
    {
        // play reload anim and wait until it finishes 
        StopShoot();
        ChangeCurrentState(FireGunState.Reloading);

        switch (totalAmmo)
        {
            case var s when weapon.InfiniteAmmo:
                currentChargedAmmo = maxChargedAmmo;
                break;
            case 0 when  currentChargedAmmo==0:
                onInsufficientAmmo?.Invoke();
                break;
            case var s when totalAmmo >= maxChargedAmmo && currentChargedAmmo == 0:
                currentChargedAmmo += maxChargedAmmo;
                totalAmmo -= maxChargedAmmo;
                Debug.Log("case 0");
                break;
            case var s when ((currentChargedAmmo + maxChargedAmmo) > maxChargedAmmo):
                var spaceAvaiable = maxChargedAmmo - currentChargedAmmo;
                if ((totalAmmo - spaceAvaiable) < 0)
                {
                    Debug.Log("change ifs");
                    goto default;
                }

                currentChargedAmmo += spaceAvaiable;
                totalAmmo -= spaceAvaiable;

                Debug.Log("case 1");
                break;

            default:

                Debug.Log("  reload all");
                currentChargedAmmo += totalAmmo;
                totalAmmo = 0;

                Debug.Log("case 2");
                break;
        }

        onReload?.Invoke(currentChargedAmmo, totalAmmo);
     }

    public virtual void Discard()
    {
        ChangeCurrentState(FireGunState.Discard);
        onDiscard?.Invoke(this);
        Debug.Log($"disposed {name }    {this.gameObject.name}");

        DestroyImmediate(this.gameObject);
    }
    
    public void StopShoot()
    {
        StopCoroutine(nameof(Fire));
        ChangeCurrentState(FireGunState.Idle);
        isFiring = false;
        //timesShoot = 0;
    }

    public void ChangeCurrentState(FireGunState newState)
    {
        previousFireGunState = currentFireGunState;
        currentFireGunState = newState;
    }

    #region Gets and Sets

    public WeaponData GetWeapon => weapon;

    public float MaxAmmo => maxAmmo;

    public int CurrentChargedAmmo => currentChargedAmmo;

    public int CurrentDamage => currentDamage;


    public bool IsFiring => isFiring;

    public FireGunState PreviousFireGunState => previousFireGunState;

    public FireGunState CurrentFireGunState => currentFireGunState;

    public int TimesShoot
    {
        get => shootCount;
        set => shootCount = value;
    }

    #endregion
}

public enum TypeOfShoot
{
    Manual,
    SemiAutomatic,
    Automatic
}

public enum FireGunState
{
    Idle = 0,
    Reloading = 1,
    Shooting = 2,
    Discard = 3
}