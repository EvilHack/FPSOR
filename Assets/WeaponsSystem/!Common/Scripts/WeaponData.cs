using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon", menuName = "New Weapon")]
public class WeaponData : ScriptableObject
{
    [SerializeField] private int damage = 10;
    [SerializeField] private float maxAmmo = 10;
    [SerializeField] private int maxAmmoPerShoot = 1;
    [SerializeField] private float recoilOnShoot = 0;
    [SerializeField] private int maxChargerAmmo = 12;
    [SerializeField] private AudioClip weaponPickUpSound;
    [SerializeField] private AudioClip weaponShootSound;
    [SerializeField] private TypeOfShoot typeOfShoot = TypeOfShoot.Manual;
    [SerializeField] private GameObject bullet;
    [SerializeField] private GameObject defaultPrefab;
    [SerializeField] private bool infiniteAmmo = false;
    private void OnValidate()
    {
        if (maxAmmo == -1)
        {
            maxAmmo = Mathf.Infinity; 
        }
    }

    #region Gets
    public int Damage => damage;

    public float MAXAmmo => maxAmmo;

    public int MAXAmmoPerShoot => maxAmmoPerShoot;

    public float RecoilOnShoot => recoilOnShoot;

    public int MAXChargerAmmo => maxChargerAmmo;

    public AudioClip WeaponPickUpSound => weaponPickUpSound;

    public AudioClip WeaponShootSound => weaponShootSound;

    public TypeOfShoot TypeOfShoot => typeOfShoot;

    public GameObject Bullet => bullet;

    public GameObject DefaultPrefab => defaultPrefab;

    public bool InfiniteAmmo => infiniteAmmo;

    

    #endregion

     void Start()
    {
    }

     void Update()
    {
    }


}