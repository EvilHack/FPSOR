using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUpWeapon : TargetHit
{
    [SerializeField] private WeaponData typeOfWeapon;

    [SerializeField] private int startingAmmo;
    public static Action<WeaponData, int> onObtainWeapon;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Awake()
    {
        onHit += OnHit;
    }

    void OnDestroy()
    {
        onHit -= OnHit;
    }

    public void OnHit(int damage)
    {
        onObtainWeapon?.Invoke(typeOfWeapon, startingAmmo);
        Destroy(this.gameObject);
    }
}