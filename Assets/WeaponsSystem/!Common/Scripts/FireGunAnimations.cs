using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(FireGun))]
public class FireGunAnimations : MonoBehaviour
{
    public Animator animator;
    private FireGun fireGun;

    [Tooltip(
        "By default the 0 element is the idle state, the 1 for the shooting state, the 2 for the reload and the 3 element is for the dispose state")]
    public List<String> animatorStateNames = new List<string>() { "Idle", "Shoot", "Reload", "Discard" };

    private void Awake()
    {
        if (animator == null)
        {
            animator = GetComponent<Animator>();
        }

        if (fireGun == null)
        {
            fireGun = GetComponent<FireGun>();
        }
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void LateUpdate()
    {
        UpdateAnimations();
    }

    private void UpdateAnimations()
    {
        switch (fireGun.CurrentFireGunState)
        {
            case FireGunState.Shooting:
                //   animator.Play(animatorStateNames[1]);

                ResetTriggersWithOneException(1);
                // if (animator.GetCurrentAnimatorStateInfo(0).IsName(animatorStateNames[1]))
                // {
                //       return;
                //  }
                animator.SetTrigger(animatorStateNames[1]);
                return;
            case FireGunState.Reloading:
                //   animator.Play(animatorStateNames[2]);   
                ReserTriggers();

                animator.SetTrigger(animatorStateNames[2]);

                return;
            case FireGunState.Discard:
                //                animator.Play(animatorStateNames[3]);         
                ReserTriggers();

                animator.SetTrigger(animatorStateNames[3]);
                fireGun.ChangeCurrentState(FireGunState.Idle);

                return;

            case FireGunState.Idle:
                //  animator.ResetTrigger(animatorStateNames[0]); 
                animator.SetTrigger(animatorStateNames[0]);

                //   animator.Play(animatorStateNames[0]);    

                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public void ReserTriggers()
    {
        animator.ResetTrigger(animatorStateNames[0]);
        animator.ResetTrigger(animatorStateNames[1]);
        animator.ResetTrigger(animatorStateNames[2]);
        animator.ResetTrigger(animatorStateNames[3]);
    }

    public void ResetTriggersWithOneException(int exception)
    {
        for (int i = 0; i < animatorStateNames.Count; i++)
        {
            if (i == exception)
            {
                continue;
            }

            animator.ResetTrigger(animatorStateNames[i]);
        }
    }
}