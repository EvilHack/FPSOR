using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class TargetHit : MonoBehaviour, IShooteable
{
    private AudioClip hitSound;
    private Camera mainCamera;
    public Action<int> onHit;
    public Action onDead;

    public int currentHealth = 1;
    public int maxHealth = 1;

    public int CurrentHealth => currentHealth;

    public int MaxHealth => maxHealth;

   

    public event Action<int, int> clientOnHealthUpdated;

    public void UpdateHealth()
    {
        clientOnHealthUpdated?.Invoke(currentHealth, maxHealth);
        if (currentHealth <= 0)
        {
            onDead?.Invoke();
        }
    }

    public void OnShootReceive(int damage)
    {
        Debug.Log("shoot really received");
        onHit?.Invoke(damage);
    }
}

public interface IShooteable
{
    void OnShootReceive(int damage);
}