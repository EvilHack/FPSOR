using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

//La municion  tiene  vida por si se quiere hacer que una munición sea mas dificil de conseguir
public class PickUpAmmo : TargetHit
{
    [SerializeField] private int quantity;
    [SerializeField] private WeaponData typeOfWeapon;

    public int Quantity => quantity;


    public static Action<PickUpAmmo> onObtainAmmo;

    void Awake()
    {
        onHit += OnHit;
    }

    void OnDestroy()
    {
        onHit -= OnHit;
    }

    public void OnHit(int damage)
    {
        onObtainAmmo?.Invoke(this);
        Destroy(this.gameObject);
    }
    public WeaponData GetTypeOfWeapon => typeOfWeapon;

}