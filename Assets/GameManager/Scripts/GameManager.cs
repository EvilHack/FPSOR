using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;
using Random = System.Random;

public class GameManager : MonoBehaviour
{
    [Range(0,100)]public float  sectionProgress;
    public List<Enemy> enemiesToSpawn =new List<Enemy>();
    public int maxNumberOfEnemies;
    public int currentNumberOfEnemies;
    public static  GameManager instance;
    private List<Enemy> runtimeEnemyList = new List<Enemy>();
    private void Awake()
    {
       
        if (instance==null)
        {

            instance = this;
            return;
        }
        Destroy(this.gameObject);
        
    }

    private void DecreaseNumberOfEnemies()
    {
        currentNumberOfEnemies--;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(nameof(StartSpawningEnemies));
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnDestroy()
    {
    }

    public IEnumerator StartSpawningEnemies()
    {
        while (maxNumberOfEnemies>currentNumberOfEnemies)
        {
         var enemy =  enemiesToSpawn[UnityEngine.Random.Range(0, enemiesToSpawn.Count)];
         Enemy instantiatedEnemy = Instantiate<Enemy>(enemy);
         instantiatedEnemy.onDead += DecreaseNumberOfEnemies;
         instantiatedEnemy.transform.position  = GetARandomPosition(10);
         runtimeEnemyList.Add(instantiatedEnemy);

         currentNumberOfEnemies++;
        }
        yield return new WaitForEndOfFrame();
    }

    public void IncreaseMaxNumberOfEnemies(int amountToIncrease)
    {
        maxNumberOfEnemies += amountToIncrease;
    }

    public Vector3 GetARandomPosition(float maxDistance)
    {
        return new Vector3(UnityEngine.Random.Range(0, maxDistance),UnityEngine.Random.Range(0, PlayerController.instance.transform.position.y),UnityEngine.Random.Range(0, maxDistance));
    }
}
