using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    private Camera mainCamera;


    private Vector3 mousePosition;

    private FireGun currentGun;

    [Header("Default Gun")] [SerializeField]
    private FireGun defaultGun;

    public static PlayerController instance;

    [Header("Y Settings")] [SerializeField]
    private float sensitivityY = 20f;

    [Header("X Settings")] [SerializeField]
    private float sensitivityX = 20f;

    private float minimumXPosition = 300f;
    private float maximumXPosition = 1700f;
    private PlayerInput playerInput;

    [Header("Cursor settings")] [SerializeField]
    private Image cursorImage;

    [SerializeField] private List<Sprite> cursorSprites = new List<Sprite>();

    [Header("Cursor mask")] public LayerMask hitMask;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }

        SetDefaultWeapon();

        playerInput = GetComponent<PlayerInput>();
        currentGun.onInsufficientAmmo += SetDefaultWeapon;
        PlayerInput.onReload += () => currentGun.Reload();
        //   FireGun.onDiscard += (FireGun f) => DisposeCurrentWeapon();
        PickUpAmmo.onObtainAmmo += PickAmmo;
        PickUpWeapon.onObtainWeapon += SetNewWeapon;
        PlayerInput.onStopGun += () => currentGun.StopShoot();
    }


    private void OnDestroy()
    {
        PlayerInput.onReload -= () => currentGun.Reload();
        PickUpAmmo.onObtainAmmo -= PickAmmo;
        //FireGun.onDiscard -= (FireGun f) => SetWeapon(defaultGun.GetWeapon);

        PlayerInput.onStopGun -= () => currentGun.StopShoot();
        currentGun.onShoot -= ShootCameraRay;
        PickUpWeapon.onObtainWeapon -= SetNewWeapon;
    }

    void Start()
    {
        mainCamera = Camera.main;
    }

    void Update()
    {
        UpdateCursorState();

        if (currentGun == null)
        {
            return;
        }

        if (playerInput.ShootPressed)
        {
            currentGun.TryToShoot();
            SetCursorState(CursorStates.center);
        }
        else
        {
            if (currentGun.GetWeapon.TypeOfShoot != TypeOfShoot.Automatic)
            {
                currentGun.TimesShoot = 0;
            }
        }

        LightOnCursorHover();
    }

    private void LightOnCursorHover()
    {
        bool raycastToCursor = Physics.Raycast(mainCamera.ScreenPointToRay(playerInput.CursorPosition),
            out RaycastHit hit,
            Mathf.Infinity, hitMask);
        if (!raycastToCursor) return;
        var itemLight = hit.collider.GetComponentInChildren<Light>();
        if (itemLight != null)
        {
            itemLight.enabled = raycastToCursor;
        }
    }

    private void UpdateCursorState()
    {
        var cursorPosition = playerInput.CursorPosition;
        var previousCursorPosition = playerInput.PreviousFrameCursorPosition;
        cursorImage.transform.position = playerInput.CursorPosition;


        bool isCursorInTheLeft = cursorPosition.x < minimumXPosition && cursorPosition.x < maximumXPosition;
        bool isCursorInTheRight = cursorPosition.x > maximumXPosition && cursorPosition.x > minimumXPosition;

        if (isCursorInTheLeft && !(cursorPosition.x > previousCursorPosition.x))
        {
            SetCursorState(CursorStates.left);
            ApplyDeltaRotationX();
        }
        else if (isCursorInTheRight && !(cursorPosition.x < previousCursorPosition.x))
        {
            SetCursorState(CursorStates.right);
            ApplyDeltaRotationX();
        }
        else
        {
            SetCursorState(CursorStates.center);
        }
    }

    public void ApplyDeltaRotationX()
    {
        Quaternion deltaRotation =
            Quaternion.AngleAxis(playerInput.Delta.x * sensitivityX * Time.deltaTime, Vector3.up);
        this.transform.rotation *= deltaRotation;
    }

    public void ApplyDeltaRotationY()
    {
        Quaternion deltaRotation =
            Quaternion.AngleAxis(playerInput.Delta.y * sensitivityY * Time.deltaTime, Vector3.right);

        this.transform.rotation *= deltaRotation;
    }

    private void PickAmmo(PickUpAmmo ammo)
    {
        if (ammo.GetTypeOfWeapon == currentGun.GetWeapon)
        {
            currentGun.IncreaseAmmo(ammo.Quantity);
            return;
        }

        Destroy(ammo.gameObject);
    }

    public void DisposeCurrentWeapon()
    {
        PlayerInput.onStopGun -= () => currentGun.StopShoot();
        FireGun.onDiscard -= (FireGun f) => SetWeapon(defaultGun.GetWeapon);

        Debug.Log($"currentgun name {currentGun.name}");
        currentGun.Discard();
    }

    private void SetDefaultWeapon()
    {
        if (currentGun != null)
            DisposeCurrentWeapon();

        SetWeapon(defaultGun.GetWeapon);
    }


    private void SetNewWeapon(WeaponData newWeapon, int startingAmmo)
    {
        if (currentGun.GetWeapon != newWeapon && currentGun.GetWeapon == defaultGun.GetWeapon)
        {
            DisposeCurrentWeapon();
            SetWeapon(newWeapon);
        }

        currentGun.IncreaseAmmo(startingAmmo);
    }

    private void SetWeapon(WeaponData weapon)
    {
        GameObject instantiatedGun = Instantiate<GameObject>(weapon.DefaultPrefab, this.transform);
        currentGun = instantiatedGun.GetComponent<FireGun>();
        Debug.Log($"currentgun name {currentGun.name}");

        currentGun.Reload();
        currentGun.onShoot += ShootCameraRay;
        PlayerInput.onStopGun += () => currentGun.StopShoot();
        currentGun.onInsufficientAmmo += SetDefaultWeapon;
    }

    private void SetCursorState(CursorStates newState)
    {
        cursorImage.sprite = cursorSprites[(int)newState];
    }

    private void ShootCameraRay()
    {
        if (!Physics.Raycast(mainCamera.ScreenPointToRay(playerInput.CursorPosition), out RaycastHit hit,
            Mathf.Infinity, hitMask)) return;
        Debug.Log("hit.collider name " + hit.collider.name);
        hit.collider.GetComponent<IShooteable>()?.OnShootReceive(currentGun.CurrentDamage);
        Debug.Log("Shoot received");
    }


    public enum CursorStates
    {
        left = 0,
        center = 1,
        right = 2
    }
}