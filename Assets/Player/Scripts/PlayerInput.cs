using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.TextCore;

public class PlayerInput : MonoBehaviour
{
    [SerializeField] private PlayerCameraActions playerActions;
    private Vector2 cursorPosition;
    private Vector2 previousFrameCursorPosition;
    private bool shootPressed;
    private Vector2 delta;
    private float timeWithoutGunInput = 0;
    public PlayerCameraActions PlayerActions => playerActions;
    public static Action onReload;
    public static Action onStopGun;

    public Vector2 CursorPosition => cursorPosition;

    public bool ShootPressed => shootPressed;

    public Vector2 Delta => delta;

    public float TimeWithoutGunInput => timeWithoutGunInput;

    public Vector2 PreviousFrameCursorPosition => previousFrameCursorPosition;


    private void Awake()
    {
        playerActions = new PlayerCameraActions();
        Cursor.visible = false;
    }

    private void OnEnable()
    {
        playerActions.Player.MoveCursor.performed += context =>
            cursorPosition = context.ReadValue<Vector2>();
        Debug.Log("camera moved I Think " + cursorPosition);


        playerActions.Player.Shoot.performed += context =>
        {
            Debug.Log("Shoot pressed");
            shootPressed = true;
            timeWithoutGunInput = 0;
        };
        playerActions.Player.Shoot.canceled += context =>
        {
            shootPressed = false;
            Debug.Log("On stop shoot");
            onStopGun?.Invoke();
            timeWithoutGunInput = 0;
        };
        playerActions.Player.Reload.performed += ReloadCurrentWeapon;
        playerActions.Player.Delta.performed += context => delta = context.ReadValue<Vector2>();
        playerActions.Player.Delta.canceled += context => delta = delta;
        ;
        playerActions.Enable();
    }

    private void ReloadCurrentWeapon(InputAction.CallbackContext obj)
    {
        Debug.Log("  Reloading");
        onReload?.Invoke();
        timeWithoutGunInput = 0;
    }

    private void OnDisable()
    {
        playerActions.Disable();
    }

    private float requiredTime = 1f;

    void Update()
    {
        timeWithoutGunInput += Time.deltaTime;
        if (timeWithoutGunInput >= requiredTime)
        {
            onStopGun?.Invoke();
            timeWithoutGunInput = 0;
        }
    }

    private void LateUpdate()
    {
        previousFrameCursorPosition = cursorPosition;
    }
}